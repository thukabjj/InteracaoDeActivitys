package com.example.arthur.interacaoactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText input;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText) findViewById(R.id.txt1);
    }
    public void Proximo (View view){

        Intent it = new Intent(this,Activity2.class);

        Bundle b = new Bundle();

        String msm = input.getText().toString();

        b.putString("txt1",msm);

        it.putExtras(b);

        startActivity(it);

    }
}
