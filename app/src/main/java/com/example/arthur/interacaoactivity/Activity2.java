package com.example.arthur.interacaoactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {
    TextView t1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        t1 = (TextView) findViewById(R.id.txt2);
        Intent it = getIntent();

        Bundle b = it.getExtras();

        String msm = b.getString("txt1");

        t1.setText(msm);

    }
}
